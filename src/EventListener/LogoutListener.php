<?php

namespace AmalricBzh\ToolsBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Event\LogoutEvent;

final class LogoutListener
{
    public function onSymfonyComponentSecurityHttpEventLogoutEvent(LogoutEvent $event): void
    {
        $response = $event->getResponse();
        if (!$response instanceof Response) {
            $response = new Response();
            $event->setResponse($response);
        }
        // Ce header empêche, après la déconnexion, d'utiliser le bouton "Back" du navigateur pour afficher les
        // pages en cache qui peuvent contenir des données sensibles.
        $response->headers->set('Clear-Site-Data', 'cache');
        $event->setResponse($response);
    }
}
