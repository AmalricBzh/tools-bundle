<?php

namespace AmalricBzh\ToolsBundle\EventListener;

use Symfony\Component\HttpKernel\Event\ResponseEvent;

final class ResponseListener
{
    public function onKernelResponse(ResponseEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }
        $response = $event->getResponse();
        $response->headers->set('X-Frame-Options', 'Deny');
    }
}
