# AmalricBzh\ToolsBundle

Ce bundle embarque plusieurs outils ou réglages communs aux sites que je développe. Il n'a pas pour but d'embarquer de fonctionnalités. Je le complèterais au fur et à mesure de mes besoins.

## Sécurité

### Protection contre le ClickJacking
Ajout d'un header "X-Frame-Options: Deny".

- https://owasp.org/www-community/attacks/Clickjacking
- https://cheatsheetseries.owasp.org/cheatsheets/Clickjacking_Defense_Cheat_Sheet.html
- https://www.riskinsight-wavestone.com/2012/12/clickjacking-mais-qui-a-vole-ma-souris/
- https://datatracker.ietf.org/doc/html/draft-ietf-websec-x-frame-options-01#section-2.1

### Protection contre la lecture du cache du navigateur après la déconnection
Ecoute de l'évènement de déconnexion, et ajout d'un header Clear-Site-Data: cache

- https://hackerone.com/reports/7909
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Clear-Site-Data


## DEV : How to use a local version of this bundle

To work locally with this bundle:


1. Create a lib subdirectory in your main project:
````
    mkdir lib
    cd lib
````

2. Go in this lib directory and git clone the project:
````
git clone https://gitlab.com/amalricbzh/tools-bundle.git
````

3. Open your main project `composer.json`
4. Replace `amalricbzh/tools-bundle` version with *@dev:

````
  "require": {
    ...
    "amalricbzh/tools-bundle": "*@dev",
    ...
   },
````
5. Add the local path in the repositories section:

````
"repositories" : [
    {
      "type": "path",
      "url": "lib/ToolsBundle",
      "options": {
        "symlink": true
      }
    }
  ]
````
6. Save the file and ask composer to use the local bundle. Open your application docker terminal and type:
````
    composer update amalricbzh/tools-bundle
````

**<span style="color:red">NB : never commit these `composer.json` modifications !</span>**


That's all, your project now use the local bundle.
